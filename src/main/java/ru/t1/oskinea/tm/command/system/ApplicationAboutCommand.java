package ru.t1.oskinea.tm.command.system;

public final class ApplicationAboutCommand extends AbstractSystemCommand {

    private static final String ARGUMENT = "-a";

    private static final String DESCRIPTION = "Show developer info.";

    private static final String NAME = "about";

    @Override
    public void execute() {
        System.out.println("[ABOUT]");
        System.out.println("Name: Evgeniy Oskin");
        System.out.println("E-mail: jizer@inbox.ru");
    }

    @Override
    public String getArgument() {
        return ARGUMENT;
    }

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public String getName() {
        return NAME;
    }

}
